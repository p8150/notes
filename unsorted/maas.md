#Install MAAS as virtual machine
##Install 

```bash
sudo hostnamectl set-hostname stas-maas-1
```

## Add swap

```bash
sudo fallocate -l 2G /swapfile
sudo mkswap /swapfile
sudo chmod 0600 /swapfile
sudo swapon /swapfile
echo '/swapfile swap swap defaults 0 0' | sudo tee -a /etc/fstab
```

## Netplan config
```yaml
network:
    ethernets:
        ens3:
            dhcp4: false
            match:
                macaddress: e2:f2:6a:01:9d:c9
            set-name: ens3
            addresses:
                - 192.168.101.254/24
            routes:
                - to: 0.0.0.0/0
                  via: 192.168.101.1
            nameservers:
                addresses: 
                    - 192.168.101.1
    version: 2
```

## Install MAAS

```bash
sudo snap install maas-test-db
sudo snap install maas
```

## Initialize MAAS

```bash
sudo maas init region+rack --database-uri maas-test-db:///
sudo maas createadmin
```