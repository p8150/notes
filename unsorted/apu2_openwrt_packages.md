# Additional packages to install on PC Engines APU2 for OpenWRT
- [Additional packages to install on PC Engines APU2 for OpenWRT](#additional-packages-to-install-on-pc-engines-apu2-for-openwrt)
  - [Packages recommended by OpenWRT](#packages-recommended-by-openwrt)
    - [APU2 front LED control](#apu2-front-led-control)
    - [AMD Cryptographic Coprocessor](#amd-cryptographic-coprocessor)
    - [GPIO pins or COM2/3/4](#gpio-pins-or-com234)
    - [Hardware watchdog](#hardware-watchdog)
    - [USB v2 and v3.0 support](#usb-v2-and-v30-support)
    - [AMD CPU microcode](#amd-cpu-microcode)
  - [All in one install](#all-in-one-install)
  - [Optional packages by OpenWRT](#optional-packages-by-openwrt)
    - [IRQ usage balancing for multi-core systems](#irq-usage-balancing-for-multi-core-systems)
    - [Onboard PC speaker](#onboard-pc-speaker)
    - [Tool to update APU BIOS](#tool-to-update-apu-bios)
    - [Discard unused blocks on SSDs](#discard-unused-blocks-on-ssds)
- [My additional packages](#my-additional-packages)
  - [Drivers](#drivers)
    - [Atheros Wi-Fi](#atheros-wi-fi)
  - [Wi-Fi](#wi-fi)
    - [wpa_supplicant](#wpa_supplicant)
    - [hostapd](#hostapd)
  - [Statistics](#statistics)
    - [collectd-mod-cpufreq](#collectd-mod-cpufreq)
    - [collectd-mod-disk](#collectd-mod-disk)
    - [collectd-mod-ping](#collectd-mod-ping)
    - [collectd-mod-sqm](#collectd-mod-sqm)
    - [collectd-mod-thermal](#collectd-mod-thermal)
    - [collectd-mod-wireless](#collectd-mod-wireless)
    - [collectd-mod-uptime](#collectd-mod-uptime)
  - [System tools](#system-tools)
    - [lm-sensors](#lm-sensors)
    - [lspci](#lspci)
    - [lsblk](#lsblk)
    - [screen](#screen)
    - [wget](#wget)
    - [curl](#curl)
  - [LUCI extensions](#luci-extensions)
    - [Wake On Lan](#wake-on-lan)
    - [Wireguard status](#wireguard-status)
    - [Statistics](#statistics-1)
  - [Network](#network)
    - [kmod-ipt-nat-extra iptables-mod-nat-extra](#kmod-ipt-nat-extra-iptables-mod-nat-extra)
    - [Wireguard](#wireguard)
    - [QoS](#qos)
    - [Let's Encrypt](#lets-encrypt)
    - [DDNS](#ddns)
    - [BanIP](#banip)
    - [OSPF](#ospf)
  - [Unsorted](#unsorted)


## Packages recommended by OpenWRT
[Openwrt Device page](https://openwrt.org/toh/pcengines/apu2)

### APU2 front LED control
```bash
opkg install kmod-leds-gpio
```

### AMD Cryptographic Coprocessor
```bash
opkg install kmod-crypto-hw-ccp
```

### GPIO pins or COM2/3/4
```bash
opkg install kmod-gpio-nct5104d
opkg install kmod-gpio-button-hotplug
```

### Hardware watchdog
```bash
opkg install kmod-sp5100_tco
```
For 21.02 use this:
```bash
opkg install kmod-sp5100-tco
```
### USB v2 and v3.0 support
```bash
opkg install kmod-usb-core kmod-usb-ohci kmod-usb2 kmod-usb3
```

### AMD CPU microcode
```bash
opkg install amd64-microcode
```

## All in one install
```bash
opkg install kmod-leds-gpio kmod-crypto-hw-ccp kmod-gpio-nct5104d kmod-gpio-button-hotplug kmod-sp5100_tco kmod-usb-core kmod-usb-ohci kmod-usb2 kmod-usb3 amd64-microcode
```

## Optional packages by OpenWRT
### IRQ usage balancing for multi-core systems
```bash
opkg install irqbalance
uci set irqbalance.irqbalance.enabled=1
uci commit
```

### Onboard PC speaker
```bash
opkg install kmod-sound-core kmod-pcspkr
```

### Tool to update APU BIOS
> WARNING: it doesn't work on OpenWRT 19.07 since there is no /dev/mem device 

```bash
opkg install flashrom
```

### Discard unused blocks on SSDs
> NOTE: to check if it should be enabled afterwards
```bash
opkg install fstrim
```

# My additional packages
## Drivers
### Atheros Wi-Fi
```bash
opkg install kmod-ath9k
```

## Wi-Fi
### wpa_supplicant
```bash
opkg install wpa_supplicant
```

### hostapd
```bash
opkg install hostapd
```


## Statistics
### collectd-mod-cpufreq
### collectd-mod-disk
### collectd-mod-ping
### collectd-mod-sqm
### collectd-mod-thermal
### collectd-mod-wireless
### collectd-mod-uptime

## System tools

### lm-sensors
```bash
opkg install lm-sensors
```

### lspci
```bash
opkg install pciutils
```
### lsblk
```bash
opkg install lsblk
```
### screen
```bash
opkg install screen
```

### wget
```bash
opkg install wget
```

### curl
```bash
opkg install curl
```

## LUCI extensions
### Wake On Lan
```bash
opkg install luci-app-wol
```

### Wireguard status
```bash
luci-app-wireguard
```

### Statistics
```bash
opkg install luci-app-statistics
```


## Network
### kmod-ipt-nat-extra iptables-mod-nat-extra

### Wireguard
```bash
opkg install luci-proto-wireguard
```

### QoS
```bash
opkg install luci-app-qos
opkg install luci-app-sqm
```

### Let's Encrypt
```bash
opkg install luci-app-acme acme-dnsapi libuhttpd-openssl luci-app-uhttpd
```

### DDNS
```bash
opkg install luci-app-ddns
```

### BanIP
```bash
opkg install luci-app-banip
```

### OSPF
```bash
opkg install quagga-ospfd quagga-zebra quagga-vtysh
```

## Unsorted
