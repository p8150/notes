# Install NVIDIA driver

1. Add RPMFusion repo

```bash
sudo rpm-ostree install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
```
2. Install NVIDIA driver

```bash
sudo rpm-ostree install akmod-nvidia xorg-x11-drv-nvidia-cuda
```
