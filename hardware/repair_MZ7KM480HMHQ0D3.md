# Repair firmware for Dell MZ7KM480HMHQ0D3 (Samsung PM863)

## Symptoms

### smartctl

```bash
sudo smartctl -a /dev/sda
smartctl 7.3 2022-02-28 r5338 [x86_64-linux-5.17.5-200.fc35.x86_64] (local build)
Copyright (C) 2002-22, Bruce Allen, Christian Franke, www.smartmontools.org

=== START OF INFORMATION SECTION ===
Model Family:     Samsung based SSDs
Device Model:     MZ7KM480HMHQ0D3
Serial Number:    S3BUNX0J603459
LU WWN Device Id: 5 002538 c4065c4fb
Add. Product Id:  DELL(tm)
Firmware Version: ERRORMOD
User Capacity:    1,006,260,224 bytes [1.00 GB]
Sector Size:      512 bytes logical/physical
Rotation Rate:    Solid State Device
Form Factor:      2.5 inches
TRIM Command:     Available, deterministic, zeroed
Device is:        In smartctl database 7.3/5319
ATA Version is:   ACS-3 T13/2161-D revision 5
SATA Version is:  SATA 3.2, 6.0 Gb/s (current: 6.0 Gb/s)
Local Time is:    Mon May  9 10:11:49 2022 CEST
SMART support is: Available - device has SMART capability.
SMART support is: Enabled

Read SMART Data failed: scsi error badly formed scsi parameters

=== START OF READ SMART DATA SECTION ===
SMART Status command failed
Please get assistance from https://www.smartmontools.org/
Register values returned from SMART Status command are:
 ERR=0x00, SC=0x00, LL=0x00, LM=0x00, LH=0x00, DEV=0x00, STS=0x50
SMART Status not supported: Invalid ATA output register values
SMART overall-health self-assessment test result: UNKNOWN!
SMART Status, Attributes and Thresholds cannot be read.

Read SMART Log Directory failed: scsi error badly formed scsi parameters

Read SMART Error Log failed: scsi error badly formed scsi parameters

Read SMART Self-test Log failed: scsi error badly formed scsi parameters

Selective Self-tests/Logging not supported

```

### Samsung utility output

```
sudo ./Samsung_SSD_DC_Toolkit_for_Linux_V2.1.exe -L
================================================================================================
Samsung DC Toolkit Version 2.1.L.Q.0
Copyright (C) 2017 SAMSUNG Electronics Co. Ltd. All rights reserved.
================================================================================================
Bad argument "name", expected an absolute path in /dev/ or /sys or a unit name: Invalid argument
-----------------------------------------------------------------------------------------------------------------------------------
| Disk   | Path     | Model           | Serial               | Firmware | Optionrom | Capacity | Drive  | Total Bytes | NVMe Driver |
| Number |          |                 | Number               |          | Version   |          | Health | Written     |             |
-----------------------------------------------------------------------------------------------------------------------------------
| 0      | /dev/sda | MZ7KM480HMHQ0D3 |       S3BUNX0J603459 | ERRORMOD | N/A       |     0 GB | N/A    | 0.00 TB     | N/A         |
-----------------------------------------------------------------------------------------------------------------------------------
```

## Flash firmware

1. [Download firmware from Dell](https://www.dell.com/support/home/en-uk/drivers/driversdetails?driverid=97d8j)
```bash
wget https://dl.dell.com/FOLDER04918071M/2/Serial-ATA_Firmware_97D8J_WN32_GD57_A00_01.EXE
```
2. ~~[Download Samsung DC Toolkit](https://semiconductor.samsung.com/consumer-storage/support/tools/). Dell has there own utility which refused to recognise the drive as valid flash target. Download windows version to extract firmware.~~ Not needed, only for some info.
```bash
wget https://semiconductor.samsung.com/resources/software-resources/Samsung_SSD_DC_Toolkit_for_Linux_V2.1.exe
chmod +x Samsung_SSD_DC_Toolkit_for_Linux_V2.1.exe
```
3. Extract firmware
```bash
sudo dnf install -y p7zip
7za x Serial-ATA_Firmware_97D8J_WN32_GD57_A00_01.EXE
mv payload/GD57_NF.fwh ./
```
4. Cut Dell metadata from firmware. Actual firmware starts from 0x00000130 (304 bytes offset). Firmware will be *exactly* 1MB (1048576 bytes) 

```bash
dd if=GD57_NF.fwh skip=1 bs=304 of=./fw_short.bin
```

```bash
hexdump GD57_NF.fwh -C -n 400
00000000  00 00 00 00 00 00 00 00  39 47 44 35 37 47 44 35  |........9GD57GD5|
00000010  30 78 03 00 00 00 00 00  00 00 00 00 00 00 00 00  |0x..............|
00000020  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
00000030  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 05  |................|
00000040  20 20 31 30 35 32 33 37  20 20 20 20 20 20 20 20  |  105237        |
00000050  20 20 20 20 20 20 20 20  20 20 20 20 20 20 20 20  |                |
00000060  20 4d 5a 37 4b 4d 32 34  30 48 4d 48 51 30 44 33  | MZ7KM240HMHQ0D3|
00000070  20 20 31 30 35 32 33 38  20 20 20 20 20 20 20 20  |  105238        |
00000080  20 20 20 20 20 20 20 20  20 20 20 20 20 20 20 20  |                |
00000090  20 4d 5a 37 4b 4d 34 38  30 48 4d 48 51 30 44 33  | MZ7KM480HMHQ0D3|
000000a0  20 20 31 30 35 32 33 39  20 20 20 20 20 20 20 20  |  105239        |
000000b0  20 20 20 20 20 20 20 20  20 20 20 20 20 20 20 20  |                |
000000c0  20 4d 5a 37 4b 4d 39 36  30 48 4d 4a 50 30 44 33  | MZ7KM960HMJP0D3|
000000d0  20 20 31 30 35 32 34 30  20 20 20 20 20 20 20 20  |  105240        |
000000e0  20 20 20 20 20 20 20 20  20 20 20 20 20 20 20 20  |                |
000000f0  20 4d 5a 37 4b 4d 31 54  39 48 4d 4a 50 30 44 33  | MZ7KM1T9HMJP0D3|
00000100  20 20 31 30 35 32 34 31  20 20 20 20 20 20 20 20  |  105241        |
00000110  20 20 20 20 20 20 20 20  20 20 20 20 20 20 20 20  |                |
00000120  20 4d 5a 37 4b 4d 33 54  38 48 4d 4c 50 30 44 33  | MZ7KM3T8HMLP0D3|
00000130  b9 69 b8 a1 7f 6e 57 ea  35 25 ed 52 cc cb 5b 8d  |.i...nW.5%.R..[.|
00000140  d2 c6 86 04 fb 38 8a c1  18 34 b5 58 3d 16 7d b6  |.....8...4.X=.}.|
00000150  52 81 6c 0b 0c 5d aa 8b  37 9d 86 58 55 69 ab 11  |R.l..]..7..XUi..|
00000160  77 2a f0 65 c9 42 c4 8a  31 38 32 30 30 32 30 33  |w*.e.B..18200203|
00000170  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
00000180  00 00 00 00 00 00 00 00  0c 24 c2 e4 00 00 00 00  |.........$......|
```
5. Flash firmware to ssd
```bash
hdparm --fwdownload fw_short.bin --yes-i-know-what-i-am-doing --please-destroy-my-drive /dev/sda
```
6. Issue secure erase
```bash
sudo hdparm --security-erase NULL /dev/sda
```
8. Power cycle
9. Escape ERRORMOD (not tested)

Note: not tried but may help as well
```
magician -d 1 -V -e
```
